<?php

function sendMail($to, $from, $subject, $message)
{
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: ' . $from . '' . "\r\n" .
        'Reply-To: ' . $from . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    return mail($to, $subject, $message, $headers);
}

function redirect($url)
{
    if (!headers_sent() && empty(ob_get_contents())) {
        header("Location: " . $url);
    }
    echo '<script>';
    echo 'window.location.href = "' . $url . '"';
    echo '</script>';
    echo '<strong>Please enable JavaScript!</strong>';
    echo '<br>';
    echo '<a href="' . $url . '">Click here to redirect manually</a>';
    exit();
}

function getRequestUriWithoutParams()
{
    return explode('?', $_SERVER["REQUEST_URI"])[0];
}

function getGetParams()
{
    $splittedUri = explode('?', $_SERVER["REQUEST_URI"]);

    if (count($splittedUri) < 2 || empty($splittedUri[1])) {
        return "?redirect=" . getRequestUriWithoutParams();
    } else {
        return "?" . $splittedUri[1] . '&redirect=' . getRequestUriWithoutParams();
    }
}