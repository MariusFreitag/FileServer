<?php

require_once 'Helper.php';

class FileBrowser
{
    private $html = '';
    private $directory;

    private $fileTemplate;
    private $folderTemplate;
    private $advancedInfoTemplate;

    public function __construct($path)
    {
        $this->fileTemplate = file_get_contents(__DIR__ . '/../static/templates/file_template.html');;
        $this->folderTemplate = file_get_contents(__DIR__ . '/../static/templates/folder_template.html');
        $this->advancedInfoTemplate = file_get_contents(__DIR__ . '/../static/templates/advancedinfo_template.html');

        $this->directory = $path;
        $this->printElements();

        PageConstructor::printPage($this->html, $this->getBreadcrumbs());
    }

    private function addHtml($html)
    {
        $this->html .= $html;
    }

    private function getBreadcrumbs()
    {
        $fakepathArray = explode("/", $this->directory->getFakePath());

        $ancestorLinkPath = "";
        $breadcrumbs = array("/" => "/");

        foreach ($fakepathArray as $fakePathPart) {

            if ($fakePathPart === "") {
                continue;
            }

            $ancestorLinkPath = $ancestorLinkPath . "/" . $fakePathPart;
            $breadcrumbs[$ancestorLinkPath] = $fakePathPart;
        }
        return $breadcrumbs;
    }

    private function printElements()
    {
        $directoryContents = (!SsoClient::isValid() && $this->directory->getPath() == "files/lib/") ? array() : scandir($this->directory->getPath());

        $files = array();
        $folders = array();

        foreach ($directoryContents as $file) {
            if ((startsWith($file, '.')) && !SsoClient::isValid()) {
                continue;
            }

            if (!SsoClient::isValid() && ($file == ".." || $file == "." || $file == ".htaccess" || ($this->directory->getPath() == "files/" && $file == "lib"))) {
                continue;
            }

            if (is_dir($this->directory->getPath() . $file)) {
                $folders[] = $file;
            } else {
                $files[] = FileClass::getByPath($this->directory->getPath() . "/" . $file);
            }
        }

        foreach ($folders as $folder) {
            $this->addFolder($folder);
        }

        foreach ($files as $file) {
            $this->addFile($file);
        }

        if (sizeof($files) === 0 && sizeof($folders) === 0) {
            $this->addHtml('<p>This folder is empty!</p>');
        }
    }

    private function addFile($file)
    {
        $advancedInfoHtml = $this->advancedInfoTemplate;
        $advancedInfoHtml = str_replace('##HREF##', $file->getFakePath() . getGetParams(), $advancedInfoHtml);
        $advancedInfoHtml = str_replace('##LOCKEDINFO##', $file->isLocked ? 'Locked' : 'Unlocked', $advancedInfoHtml);
        $advancedInfoHtml = str_replace('##LOCKACTION##', $file->isLocked ? 'unlock' : 'lock', $advancedInfoHtml);
        $advancedInfoHtml = str_replace('##HIDDENINFO##', $file->isHidden() ? 'Hidden' : 'Unhidden', $advancedInfoHtml);
        $advancedInfoHtml = str_replace('##HIDEACTION##', $file->isHidden() ? 'unhide' : 'hide', $advancedInfoHtml);

        $fileHtml = $this->fileTemplate;
        $fileHtml = str_replace('##HREF##', $file->getFakePath() . getGetParams(), $fileHtml);
        $fileHtml = str_replace('##FILENAME##', $file->name, $fileHtml);
        $fileHtml = str_replace('##DOWNLOADCOUNT##', $file->downloadCounter, $fileHtml);
        $fileHtml = str_replace('##FILESIZE##', formatByteSize($file->size), $fileHtml);
        $fileHtml = str_replace('##ADVANCEDINFO##', SsoClient::isValid() ? $advancedInfoHtml : '', $fileHtml);

        $this->addHtml($fileHtml . "\n");
    }

    private function addFolder($folder)
    {
        $location = ($this->directory->getFakePath() != "" ? "/" : "") . $this->directory->getFakePath() . '/' . $folder;

        $folderHtml = $this->folderTemplate;
        $folderHtml = str_replace('##HREF##', $location . getGetParams(), $folderHtml);
        $folderHtml = str_replace('##FILENAME##', $folder, $folderHtml);

        $this->addHtml($folderHtml . "\n");
    }
}