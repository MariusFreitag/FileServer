<?php

class SsoClient
{
    private static $apiPath = "https://accounts.mariusfreitag.de";

    private static $isValid = null;

    public static function initialize()
    {
        if (isset($_GET["token"])) {
            $_SESSION["token"] = $_GET['token'];
            header('Location: /');
            exit();
        }
    }

    public static function isValid()
    {
        if (isset(self::$isValid)) {
            return self::$isValid;
        }

        if (!isset($_SESSION["token"])) {
            return false;
        }

        try {
            self::$isValid = json_decode(@file_get_contents(SsoClient::$apiPath . '/verify/' . $_SESSION["token"]));
        } catch (Exception $exception) {
            self::$isValid = false;
        }

        return self::$isValid;
    }

    public static function login()
    {
        header('Location: ' . SsoClient::$apiPath . '/?redirect=' . $_SERVER['HTTP_HOST']);
        exit();
    }

    public static function logout()
    {
        try {
            @file_get_contents(SsoClient::$apiPath . '/invalidate/' . $_SESSION["token"]);
        } catch (Exception $exception) {
        }

        $_SESSION["token"] = null;
        header('Location: /');
        exit();
    }
}

SsoClient::initialize();