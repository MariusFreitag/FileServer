<?php

require_once 'Helper.php';

class DBHelper
{
    private static $connection = null;

    private static $dbPath = 'persistence/database.sqlite3';

    private static $createTableStatement = <<<EOT
        CREATE TABLE IF NOT EXISTS files (
          id        INTEGER       PRIMARY KEY,
          path      TEXT          NOT NULL UNIQUE,
          downloads INTEGER       NOT NULL DEFAULT '0',
          locked    INTEGER       NOT NULL DEFAULT '0'
        );
EOT;


    private static function getConnection()
    {
        // Create persistence directory if it does not exist
        if (!file_exists(dirname(self::$dbPath))) {
            mkdir(dirname(self::$dbPath), 0777, true);
        }

        if (self::$connection == null) {
            // Open connection to file database
            self::$connection = new SQLite3(self::$dbPath);

            // Create files table if it doesn't exist
            self::$connection->exec(self::$createTableStatement);
        }

        return self::$connection;
    }

    public static function exec($query)
    {
        return self::getConnection()->exec($query);
    }

    public static function query($query)
    {
        return self::getConnection()->query($query);
    }
}