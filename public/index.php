<?php
require_once 'php/Helper.php';

if (isset($_GET["debug"])) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

// Create files directory if it does not exist
if (!file_exists('files')) {
    mkdir('files', 0777, true);
}

$app = new \Slim\Slim();

$app->get('/', function () {
    new FileBrowser(new DirectoryClass(""));
});
$app->get('/:name+', function ($name) use ($app) {
    $specialMethods = array("session", "forbidden", "login", "logout", "md5", "hide", "unhide", "lock", "unlock", "optimize");

    if (in_array($name[0], $specialMethods)) {
        $app->pass();
    }

    $rawpath = implode("/", $name);

    $directory = new DirectoryClass($rawpath);
    $file = FileClass::getByPath($rawpath);

    #region Provide Download
    $fileVariations = [];
    // File
    $fileVariations[] = $file;
    // File in its hidden version
    $fileVariations[] = FileClass::getByPath($file->directory->getPath() . "." . $file->name);
    // File in its unhidden version
    $fileVariations[] = FileClass::getByPath($file->directory->getPath() . "." . substr($file->name, 0, strlen($file->name) - 1));

    foreach ($fileVariations as $file) {
        if ($file->exists) {
            // check locked
            if ($file->isLocked) {
                echo '<script>window.alert("The file ' . $file->getFakePath() . ' is locked!")</script>';
                redirect('/');
            }

            // check already downloaded
            if (!array_key_exists($file->getFakePath(), $_SESSION['downloaded'])) {
                $file->increaseDownloadCounter();
                $_SESSION['downloaded'][$file->getFakePath()] = '';
            }

            // download
            $file->provideDownload();
        }
    }
    #endregion

    // alert when directory doesn't exist
    if (!$directory->exists && !$file->exists) {
        echo '<script>window.alert("The ' . (contains($directory->getPath(), '.') ? 'file' : 'folder') . ' /' . $directory->getFakePath() . ' does not exist!")</script>';
        redirect('/');
    }

    // Redirect if request uri is malformed
    if ("/" . $directory->getFakePath() != getRequestUriWithoutParams()) {
        redirect("/" . $directory->getFakePath() . getGetParams());
    }

    new FileBrowser($directory);
});

$app->get('/session', function () {
    var_dump($_SESSION);
});

$app->get('/forbidden', function () {
    SsoClient::login();
});

$app->get('/login', function () {
    SsoClient::login();
});
$app->get('/logout', function () {
    SsoClient::logout();
});

$app->get('/md5', function () {
    echo "File does not exist";
});
$app->get('/md5/:name+', function ($name) {
    $file = FileClass::getByPath(implode("/", $name));
    if ($file->exists) {
        echo $file->calculateMd5Hash();
    } else {
        redirect("/md5");
    }
});

$app->get('/hide', function () {
    echo "File does not exist";
});
$app->get('/hide/:name+', function ($name) {
    $file = FileClass::getByPath(implode("/", $name));
    $alreadyHidden = $file->isHidden();

    if (!SsoClient::isValid()) {
        redirect("/forbidden" . getGetParams());
    }

    if (!$file->exists) {
        redirect("/hide");
    }

    $file->hide(true);

    echo "<strong>" . ($file->isHidden() ? "success" : "failure") . "</strong>";

    if ($file->isHidden() && !$alreadyHidden) {
        sendMail("admin@files.mariusfreitag.de", "notification@files.mariusfreitag.de", "FileServer Notification (/hide)", "File <strong>'" . $file->getFakePath() . "'</strong> was </strong>hidden</strong>!");
    }

    if (isset($_GET['redirect'])) {
        redirect($_GET['redirect']);
    }
});
$app->get('/unhide/:name+', function ($name) {

    $implodedName = implode("/", $name);

    if (endsWith($implodedName, "/")) {
        redirect(substr($implodedName, 0, strlen($implodedName) - 2));
    }

    $file = FileClass::getByPath(implode("/", $name));
    $alreadyHidden = $file->isHidden();

    if (!SsoClient::isValid()) {
        redirect("/forbidden?redirect=/unhide");
    }

    if (!$file->exists) {
        redirect("/hide");
    }

    $file->hide(false);

    echo "<strong>" . (!$file->isHidden() ? "success" : "failure") . "</strong>";

    if (!$file->isHidden() && $alreadyHidden) {
        sendMail("admin@files.mariusfreitag.de", "notification@files.mariusfreitag.de", "FileServer Notification (/unhide)", "File <strong>'" . $file->getFakePath() . "'</strong> was </strong>unhidden</strong>!");
    }

    if (isset($_GET['redirect'])) {
        redirect($_GET['redirect']);
    }

});

$app->get('/lock', function () {
    if (!SsoClient::isValid()) {
        redirect("/forbidden?redirect" . getGetParams());
    }

    $breadcrumbs = array('/' => '/', '/lock' => 'lock');

    $htmlContent = '<p><strong>Locked files:</strong></p>';

    $result = DBHelper::query("SELECT * FROM files WHERE locked = 1");
    if ($result != false) {
        while ($row = $result->fetchArray()) {
            $htmlContent .= '<p>' . $row["path"] . ' (<a href="/unlock/' . $row["path"] . '?redirect=/lock">unlock</a>)' . "</p>";
        }
    }

    PageConstructor::printPage($htmlContent, $breadcrumbs);
});
$app->get('/lock/:name+', function ($name) {
    $file = FileClass::getByPath(implode("/", $name));
    $alreadyLocked = $file->isLocked;

    if (!SsoClient::isValid()) {
        redirect("/forbidden" . getGetParams());
    }

    if (!$file->exists) {
        echo "File does not exist";
        exit();
    }

    $file->lock();

    echo "<strong>" . ($file->isLocked ? "success" : "failure") . "</strong>";

    if ($file->isLocked && !$alreadyLocked) {
        sendMail("admin@files.mariusfreitag.de", "notification@files.mariusfreitag.de", "FileServer Notification (/lock)", "File <strong>'" . $file->getFakePath() . "'</strong> was </strong>locked</strong>!");
    }

    if (isset($_GET['redirect'])) {
        redirect($_GET['redirect']);
    }
});

$app->get('/unlock', function () {
    redirect("/lock");
});
$app->get('/unlock/:name+', function ($name) {
    $file = FileClass::getByPath(implode("/", $name));
    $alreadyUnlocked = !$file->isLocked;

    if (!SsoClient::isValid()) {
        redirect("/forbidden" . getGetParams());
    }

    if (!$file->exists) {
        echo "File does not exist";
        exit();
    }

    $file->unlock();

    echo "<strong>" . (!$file->isLocked ? "success" : "failure") . "</strong>";

    if (!$file->isLocked && !$alreadyUnlocked) {
        sendMail("admin@files.mariusfreitag.de", "notification@files.mariusfreitag.de", "FileServer Notification (/unlock)", "File <strong>'" . $file->getFakePath() . "'</strong> was </strong>unlock</strong>!");
    }

    if (isset($_GET['redirect'])) {
        redirect($_GET['redirect']);
    }
});

$app->get('/optimize', function () {

    if (!SsoClient::isValid()) {
        redirect("/forbidden" . getGetParams());
    }

    $result = DBHelper::query("SELECT * FROM files");

    $count = 0;
    if ($result != false) {
        while ($row = $result->fetchArray()) {
            $fileClass = FileClass::getByPath($row['path']);
            if (!$fileClass->exists) {
                if (DBHelper::exec("DELETE FROM files WHERE path = '" . $row['path'] . "'")) {
                    $count++;
                };
            }
        }
    }
    echo "<p><strong>$count</strong> entries deleted!</strong></p>";


    if (isset($_GET['redirect'])) {
        redirect($_GET['redirect']);
    }
});

$app->get('/:name/', function ($name) {
    redirect("/" . $name);
});

$app->run();