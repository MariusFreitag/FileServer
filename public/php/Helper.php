<?php

session_set_cookie_params(2592000);
session_start();

if (!isset($_SESSION['downloaded'])) {
    $_SESSION['downloaded'] = array();
}

require_once './vendor/autoload.php';
require_once 'DBHelper.php';
require_once 'FileClass.php';
require_once 'DirectoryClass.php';
require_once 'SsoClient.php';
require_once 'PageConstructor.php';
require_once 'FileBrowser.php';
require_once 'StringHelper.php';
require_once 'WebHelper.php';

function formatByteSize($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
}