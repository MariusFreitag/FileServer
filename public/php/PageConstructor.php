<?php

require_once 'Helper.php';

class PageConstructor
{
    public static function printPage($content, $breadcrumbs)
    {
        $html = file_get_contents(utf8_encode(__DIR__ . '/../static/templates/page_template.html'));
        $html = str_replace('##NAV##', SsoClient::isValid() ? (file_get_contents(__DIR__ . '/../static/templates/nav_template.html')) : '', $html);
        $html = str_replace('##MAIN##', utf8_encode($content), $html);
        $html = str_replace('##GETGETPARAMS##', utf8_encode(getGetParams()), $html);

        $breadcrumbHtml = '';
        foreach ($breadcrumbs as $key => $value) {
            $breadcrumbHtml .= "<a href=\"$key\">$value</a>\n";
        }
        $html = str_replace('##BREADCRUMBS##', utf8_encode($breadcrumbHtml), $html);

        echo $html;
    }
}