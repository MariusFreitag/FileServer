<?php

require_once 'Helper.php';

class DirectoryClass
{
    private $path = "";
    public $exists = true;

    public function __construct($path)
    {
        $path = $path == "." ? "" : $path;
        $this->path = startsWith($path, "files") ? $path : ("files/" . $path);
        $this->correctPath();
        $this->exists = is_dir($this->path) ? true : false;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getFakePath()
    {
        return substr($this->path, 6, strlen($this->path) - 6 - 1);
    }

    private function correctPath()
    {
        while (contains($this->path, "//")) {
            $this->path = str_replace("//", "/", $this->path);
        }

        if (startsWith($this->path, "/")) {
            $this->path = substr($this->path, 1);
        }

        if (!endsWith($this->path, "/")) {
            $this->path .= "/";
        }
    }
}