FROM php:apache

WORKDIR /var/www/html

EXPOSE 80

# Install extensions
RUN rm /etc/apt/preferences.d/no-debian-php
RUN apt-get update && \
    apt-get install --assume-yes sendmail php7.0-sqlite3

# Install PHP dependencies with Composer
COPY ./public/composer.json ./composer.json
RUN apt-get install --assume-yes git zlib1g-dev && \
    docker-php-ext-install zip && \
    curl -sS https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php && \
    php composer.phar install && \
    rm -f composer-setup.php && \
    rm -f composer.phar

RUN a2enmod rewrite

COPY ./public ./
