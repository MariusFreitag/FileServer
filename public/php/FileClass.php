<?php

require_once 'Helper.php';

class FileClass
{
    public $id = -1;
    public $name;
    public $directory;
    public $downloadCounter = 0;
    public $isLocked = false;
    // in Byte
    public $size;
    public $contentType;
    public $exists;

    private function __construct($filePath)
    {
        $this->initialize($filePath);
    }

    private function initialize($filePath)
    {
        $this->directory = new DirectoryClass(dirname($filePath));
        $this->name = basename($filePath);
        $this->exists = is_file($this->getPath()) ? true : false;

        $query = "SELECT * FROM files WHERE path='" . $this->getPath() . "'";
        $result = DBHelper::query($query)->fetchArray();

        if ($result != false) {
            $this->id = $result['id'];
            $this->downloadCounter = $result['downloads'];
            $this->isLocked = $result['locked'];
        } else if ($this->exists) {
            DBHelper::exec("INSERT INTO files (path) VALUES ('" . $this->getPath() . "')");
        }

        if ($this->exists) {
            $this->size = filesize($this->getPath());
            #region Set ContentType
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $this->contentType = finfo_file($finfo, $this->getPath());
            finfo_close($finfo);
            #endregion
        }
    }

    public static function getByPath($filePath)
    {
        return new FileClass($filePath);
    }

    public function getPath()
    {
        return $this->directory->getPath() . $this->name;
    }

    public function getFakePath()
    {
        return substr($this->directory->getPath() . $this->name, 6);
    }

    public function isHidden()
    {
        return startsWith($this->name, ".");
    }

    public function hide($hide)
    {
        $oldPath = $this->getPath();
        if ($hide) {
            $newPath = $this->directory->getPath() . "." . $this->name;
            if (!$this->isHidden()) {
                $this->name = "." . $this->name;
            }
        } else {
            $newPath = $this->directory->getPath() . substr($this->name, 1);
            if ($this->isHidden()) {
                $this->name = substr($this->name, 1);
            }
        }
        rename($oldPath, $newPath);

        $query = "UPDATE files SET path = '$newPath' WHERE id='" . $this->id . "'";
        DBHelper::exec($query);

        $this->initialize($newPath);
    }

    public function provideDownload()
    {
        ini_set('memory_limit', '300M');

        $downloadFileName = $this->isHidden() ? substr($this->name, 1) : $this->name;

        header('Content-Type: ' . $this->contentType);
        header('Content-Length: ' . $this->size);
        header('Content-Disposition: attachment; filename="' . $downloadFileName . '"');
        
        readfile($this->getPath());
        exit();
    }

    public function calculateMd5Hash()
    {
        return md5_file($this->getPath());
    }

    public function lock()
    {
        $query = "UPDATE files SET locked = 1 WHERE id='" . $this->id . "'";
        DBHelper::exec($query);

        $this->initialize($this->getPath());
    }

    public function unlock()
    {
        $query = "UPDATE files SET locked = 0 WHERE id='" . $this->id . "'";
        DBHelper::exec($query);

        $this->initialize($this->getPath());
    }

    function increaseDownloadCounter()
    {
        $query = "UPDATE files SET downloads = downloads + 1 WHERE id = '" . $this->id . "'";
        DBHelper::exec($query);

        $this->initialize($this->getPath());
    }
}